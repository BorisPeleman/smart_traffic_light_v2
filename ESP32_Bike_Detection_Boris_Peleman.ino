#include <Bike_detection_V2_inferencing.h>

const int BIKE_GREEN_PIN = 2;
const int BIKE_YELLOW_PIN = 3;
const int BIKE_RED_PIN = 4;
const int CAR_GREEN_PIN = 5;
const int CAR_YELLOW_PIN = 6;
const int CAR_RED_PIN = 7;

static const float features[] = {
    // copy raw features here (for example from the 'Live classification' page)
    // see https://docs.edgeimpulse.com/docs/running-your-impulse-arduino
};

/**
 * @brief      Copy raw feature data in out_ptr
 *             Function called by inference library
 *
 * @param[in]  offset   The offset
 * @param[in]  length   The length
 * @param      out_ptr  The out pointer
 *
 * @return     0
 */
int raw_feature_get_data(size_t offset, size_t length, float *out_ptr) {
    memcpy(out_ptr, features + offset, length * sizeof(float));
    return 0;
}

void print_inference_result(ei_impulse_result_t result) {
    // function implementation here
}

/**
 * @brief      Arduino setup function
 */
void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    // comment out the below line to cancel the wait for USB connection (needed for native USB)
    while (!Serial);
    Serial.println("Edge Impulse Inferencing Demo");
  
    pinMode(BIKE_GREEN_PIN, OUTPUT);
    pinMode(BIKE_YELLOW_PIN, OUTPUT);
    pinMode(BIKE_RED_PIN, OUTPUT);
    pinMode(CAR_GREEN_PIN, OUTPUT);
    pinMode(CAR_YELLOW_PIN, OUTPUT);
    pinMode(CAR_RED_PIN, OUTPUT);
}

/**
 * @brief      Arduino main function
 */
void loop()
{
    ei_printf("Edge Impulse standalone inferencing (Arduino)\n");

    if (sizeof(features) / sizeof(float) != EI_CLASSIFIER_DSP_INPUT_FRAME_SIZE) {
        ei_printf("The size of your 'features' array is not correct. Expected %lu items, but had %lu\n",
            EI_CLASSIFIER_DSP_INPUT_FRAME_SIZE, sizeof(features) / sizeof(float));
        delay(1000);
        return;
    }

    ei_impulse_result_t result = { 0 };

    // the features are stored into flash, and we don't want to load everything into RAM
    signal_t features_signal;
    features_signal.total_length = sizeof(features) / sizeof(features[0]);
    features_signal.get_data = &raw_feature_get_data;

    // invoke the impulse
    EI_IMPULSE_ERROR res = run_classifier(&features_signal, &result, false /* debug */);
    if (res != EI_IMPULSE_OK) {
        ei_printf("ERR: Failed to run classifier (%d)\n", res);
        return;
    }

    // print inference return code
    ei_printf("run_classifier returned: %d\r\n", res);
    print_inference_result(result);
  
    float threshold = 0.5;
    int numClasses = sizeof(result.classification) / sizeof(result.classification[0]);
  
    for (int i = 0; i < numClasses; i++) {
        float value = result.classification[i].value;
        String label = result.classification[i].label;

        if (value > threshold) {
          if (label.equals("Bike")) {
                digitalWrite(BIKE_GREEN_PIN, HIGH);
                digitalWrite(BIKE_YELLOW_PIN, LOW);
                digitalWrite(BIKE_RED_PIN, LOW);
                digitalWrite(CAR_GREEN_PIN, LOW);
                digitalWrite(CAR_YELLOW_PIN, LOW);
                digitalWrite(CAR_RED_PIN, HIGH);
                Serial.println("Detected bike!");
          } else if (label.equals("Other")) {
                digitalWrite(BIKE_GREEN_PIN, LOW);
                digitalWrite(BIKE_YELLOW_PIN, LOW);
                digitalWrite(BIKE_RED_PIN, HIGH);
                digitalWrite(CAR_GREEN_PIN, HIGH);
                digitalWrite(CAR_YELLOW_PIN, LOW);
                digitalWrite(CAR_RED_PIN, LOW);
                Serial.println("Detected car!");
              }
        } else {
                digitalWrite(BIKE_GREEN_PIN, LOW);
                digitalWrite(BIKE_YELLOW_PIN, HIGH);
                digitalWrite(BIKE_RED_PIN, LOW);
                digitalWrite(CAR_GREEN_PIN, LOW);
                digitalWrite(CAR_YELLOW_PIN, HIGH);
                digitalWrite(CAR_RED_PIN, LOW);
                Serial.println("Unknown object detected.");
              }
        }
  {   
  delay(1000);
  }
}
